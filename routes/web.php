<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace' => 'Auth'], function () {
    Route::get('login', 'LoginController@showLoginForm')->name('login');
    Route::post('login', 'LoginController@login');
    Route::post('logout', 'LoginController@logout')->name('logout');
});

Route::get('/', 'HomeController@home');

Route::get('/menus', 'HomeController@search');
Route::get('/menus/{menu}/detail', 'HomeController@show');
Route::get('/menus/{category}', 'HomeController@index');
Route::get('/menus/{category}/{type}', 'HomeController@index');
Route::get('/specials', 'HomeController@specials');
Route::get('/checkout', 'HomeController@checkout');
Route::post('/payment', 'HomeController@payment');
Route::get('/order', 'HomeController@order');
Route::post('/print', 'HomeController@printInvoice');

Route::group(['middleware' => 'auth'], function () {

    Route::group(['middleware' => 'admin'], function () {

        Route::get('/admin/dashboard', 'HomeController@dashboard');

        Route::get('/admin/menus', 'MenusController@index');
        Route::post('/admin/menus', 'MenusController@store');
        Route::get('/admin/menus/create', 'MenusController@create');
        Route::get('/admin/menus/{menu}', 'MenusController@show');
        Route::get('/admin/menus/{menu}/stock', 'MenusController@updateStock');
        Route::get('/admin/menus/{menu}/special', 'MenusController@specialToday');
        Route::get('/admin/menus/{menu}/edit', 'MenusController@edit');
        Route::put('/admin/menus/{menu}', 'MenusController@update');
        Route::delete('/admin/menus/{menu}', 'MenusController@destroy');

        Route::get('/categories/{category}', 'MenusController@getCategories');

        Route::get('/admin/recommendations', 'RecommendationsController@index');
        Route::get('/admin/calculate', 'RecommendationsController@calculate');
        Route::get('/admin/calculate2', 'RecommendationsController@calculate2');

        Route::get('/admin/reports', 'ReportsController@index');
        Route::get('/admin/reports/detail', 'ReportsController@detail');

    });

    Route::get('/admin/orders', 'OrdersController@index');
    Route::get('/admin/orders/{order}/process', 'OrdersController@process');
    Route::get('/admin/orders/{order}/done', 'OrdersController@orderDone');
    Route::get('/admin/orders/{order}/menus/{menu}/done', 'OrdersController@menuDone');
    Route::get('/admin/orders/{order}/menus/{menu}/undone', 'OrdersController@menuUndone');
    Route::get('/admin/orders/{order}/menus/{menu}/cancel', 'OrdersController@menuCancel');
    Route::get('/admin/orders/{order}', 'OrdersController@show');
    Route::get('/admin/orders/{order}/menus', 'OrdersController@menus');
    Route::get('/admin/orders/{order}/menus/{menu}', 'OrdersController@menusCreate');
    Route::post('/admin/orders/{order}/menus/{menu}', 'OrdersController@menusStore');
    Route::get('/admin/orders/{order}/menus/{menu}/edit', 'OrdersController@menusEdit');
    Route::put('/admin/orders/{order}/menus/{menu}', 'OrdersController@menusUpdate');

    Route::get('/admin/transactions', 'TransactionsController@index');
    Route::get('/admin/transactions/{order}', 'TransactionsController@show');

});

<?php

return [
    'categories' => [
        'main-course' => [
            'title' => 'Main Course',
            'types' => [
                'chicken' => 'Chicken',
                'seafood' => 'Seafood',
                'beef' => 'Beef',
            ]
        ],
        'appetizer' => [
            'title' => 'Appetizer',
            'types' => null,
        ],
        'dessert' => [
            'title' => 'Dessert',
            'types' => null,
        ],
        'drinks' => [
            'title' => 'Drinks',
            'types' => [
                'mocktail' => 'Mocktail',
                'cocktail' => 'Cocktail',
                'juice' => 'Juice',
                'cold-drink' => 'Cold Drink',
                'hot-drink' => 'Hot Drink',
            ]
        ],
    ],

    'status' => [
        'Baru ditambahkan',
        'Sedang diproses',
        'Selesai',
    ],

    'order_status' => [
        'Sedang diproses',
        'Selesai',
    ],
];

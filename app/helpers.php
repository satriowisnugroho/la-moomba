<?php

use Carbon\Carbon;

function price($amount)
{
    return 'Rp.' . number_format($amount, 0, '', '.') . ',-';
}

function getOrderStatus($status) {
    return config('moomba')['order_status'][$status];
}

function getCategories() {
    return config('moomba')['categories'];
}

function prettyDate($date)
{
    return (new Carbon($date, 'Asia/Jakarta'))->formatLocalized('%A, %e %B %Y');
}

function prettyTime($date)
{
    return (new Carbon($date, 'Asia/Jakarta'))->formatLocalized('%H:%M');
}


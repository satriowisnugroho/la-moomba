<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Order;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function index(Request $request)
    {
        $q = $request->get('q');

        $orders = Order::whereNotIn('status', [2])->orderBy('created_at', 'desc');

        if ($q) {
            $orders = $orders->where('name', 'like', "%$q%");
        }

        $orders = $orders->paginate();

        return view('admin.orders.index', compact('orders'));
    }

    public function show(Order $order)
    {
        $totalPrice = 0;
        foreach ($order->menus as $menu) {
            $totalPrice += $menu->pivot->total_price;
        }

        return view('admin.orders.show', compact('order', 'totalPrice'));
    }

    public function process(Order $order)
    {
        $order->fill(['status' => 1])->save();
        flash()->success('Pesanan sedang diproses');

        return redirect()->back();
    }

    public function orderDone(Order $order)
    {
        $order->fill(['status' => 2])->save();
        flash()->success('Pesanan sudah selesai');

        return redirect()->back();
    }

    public function menuDone(Order $order, Menu $menu)
    {
        $order->menus()->updateExistingPivot($menu->id, ['is_done' => true]);

        return redirect()->back();
    }

    public function menuUndone(Order $order, Menu $menu)
    {
        $order->menus()->updateExistingPivot($menu->id, ['is_done' => false]);

        return redirect()->back();
    }

    public function menuCancel(Order $order, Menu $menu)
    {
        $order->menus()->detach($menu);
        flash()->success('Pesanan menu sudah di batalkan');

        return redirect()->back();
    }

    public function menus(Request $request, Order $order)
    {
        $q = $request->get('q');

        $ids = $order->menus->map(function ($menu) {
            return $menu->id;
        });

        $menus = Menu::where('is_out', false)->whereNotIn('id', $ids);

        if ($q) {
            $menus = $menus->where('name', 'like', "%$q%");
        }

        $menus = $menus->paginate();

        return view('admin.orders.menus', compact('menus', 'order'));
    }

    public function menusCreate(Order $order, Menu $menu)
    {
        return view('admin.orders.menus-create', compact('menu', 'order'));
    }

    public function menusStore(Order $order, Menu $menu, Request $request)
    {
        $menu->orders()->attach([
            $order->id => [
                'qty' => $request->get('qty'),
                'note' => $request->get('note'),
                'price' => $menu->price,
                'total_price' => $menu->price * $request->get('qty'),
            ]
        ]);
        $menu->count_sold += $request->get('qty');
        $menu->save();

        flash()->success('Pesanan berhasil ditambahkan');

        return redirect("admin/orders/$order->id");
    }

    public function menusEdit(Order $order, Menu $menu)
    {
        $menu = $order->menus->where('id', $menu->id)->first();

        return view('admin.orders.menus-edit', compact('menu', 'order'));
    }

    public function menusUpdate(Order $order, Menu $menu, Request $request)
    {
        $order->menus()->updateExistingPivot($menu->id, [
            'qty' => $request->get('qty'),
            'note' => $request->get('note'),
        ]);

        flash()->success('Pesanan berhasil diperbarui');

        return redirect("admin/orders/$order->id");
    }
}

<?php

namespace App\Http\Controllers;

use App\Order;
use App\Recommendation;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class RecommendationsController extends Controller
{
    public function index()
    {
        $recommendations = Recommendation::paginate();

        return view('admin.recommendations', compact('recommendations'));
    }

    public function calculate()
    {
        $items = new Collection();
        $transactions = Order::all()->map(function ($transaction) use ($items) {
            $transaction->menus->each(function($menu) use ($items) {
                $found = $items->filter(function ($item) use ($menu) {
                    return $item->id == $menu->id;
                });
                if (!$found->count()) {
                    $items->push($menu);
                }
            });
            return $transaction;
        });

        DB::table('recommendations')->delete();

        $this->store($items, $transactions);

        return view('admin.calculate', compact('transactions', 'items'));
    }

    public function calculate2()
    {
        //<!-- item yang dibeli -->
        // fungsi untuk mengambil item yang dibeli

        // sama dengan array
        $items = new Collection();

        // ambil seluruh transaksi/order
        $transactions = Order::all()->map(function ($transaction) use ($items) {
            // lakukan perulangan di setiap menu/item yang dibeli
            $transaction->menus->each(function($menu) use ($items) {
                $found = $items->filter(function ($item) use ($menu) {
                    return $item->id == $menu->id;
                });
                if (!$found->count()) {
                    $items->push($menu);
                }
            });
            return $transaction;
        });
        //<!-- item yang dibeli -->

        // perulangan untuk semua item yang dibeli, untuk mendapatkan kombinasi item
        for ($i = 0; $i < $items->count() - 1; $i++) {
            for ($j = $i + 1; $j < $items->count(); $j++) {
                echo $items[$i]->name . ' ' . $items[$j]->name;
                echo '<br>';
                $t1 = 0;
                $t2 = 0;
                $total = 0;

                // perulangan transaksi untuk mengecek apakah transaksi itu memiliki item
                foreach ($transactions as $key => $transaction) {
                    $found1 = $transaction['menus']->filter(function($menu) use ($items, $i) {
                        return $menu->id == $items[$i]->id;
                    });
                    $found2 = $transaction['menus']->filter(function($menu) use ($items, $j) {
                        return $menu->id == $items[$j]->id;
                    });

                    // menampilkan jumlah item yang dibeli
                    echo $found1->count() . ' ' . $found2->count();

                    // menampilkan status
                    echo $found1->count() && $found1->count() == $found2->count() ?  ' P' : ' S';

                    // single line if else di atas sama dengan contoh if else di bawah ini :
//                    if ($found1->count() && $found1->count() == $found2->count()) {
//                        echo ' P';
//                    } else {
//                        echo ' S';
//                    }

                    $t1 += $found1->count();
                    $t2 += $found2->count();
                    $total += $found1->count() && $found1->count() == $found2->count() ? 1 : 0;

                    // $total += 1 sama dengan $total = $total + 1
                    echo '<br>';
                }

                echo 'T ' . $total;
                echo '<br>';
                echo '<br>';

                echo 'Jika beli <b>' . $items[$i]->name . '</b> maka beli <b>' . $items[$j]->name . '</b>';
                echo '<br>';
                echo "Support : ($total / " . $transactions->count() . ') * 100 = ' . ($total / $transactions->count()) * 100;
                echo '<br>';
                echo "Confidence : ($total / " . $t1 . ') * 100 = ' . ($total / $t1) * 100;
                echo '<br>';
                echo '<br>';

                echo 'Jika beli <b>' . $items[$j]->name . '</b> maka beli <b>' . $items[$i]->name . '</b>';
                echo '<br>';
                echo "Support : ($total / " . $transactions->count() . ') * 100 = ' . ($total / $transactions->count()) * 100;
                echo '<br>';
                echo "Confidence : ($total / " . $t2 . ') * 100 = ' . ($total / $t2) * 100;

                echo '<br>';
                echo '<br>';
                echo '<br>';
                echo '<br>';
                echo '<br>';
            }
        }
    }

    public function store($items, $transactions)
    {
        for ($i = 0; $i < $items->count() - 1; $i++) {
            for ($j = $i + 1; $j < $items->count(); $j++) {
                $t1 = 0;
                $t2 = 0;
                $total = 0;
                foreach ($transactions as $key => $transaction) {
                    $found1 = $transaction['menus']->filter(function($menu) use ($items, $i) {
                        return $menu->id == $items[$i]->id;
                    });
                    $found2 = $transaction['menus']->filter(function($menu) use ($items, $j) {
                        return $menu->id == $items[$j]->id;
                    });
                    $t1 += $found1->count();
                    $t2 += $found2->count();
                    $total += $found1->count() && $found1->count() == $found2->count() ? 1 : 0;
                }

                if ($total === 0) {
                    continue;
                }

                $recommendation = new Recommendation();
                $recommendation->fill([
                    'antecedent_id' => $items[$i]->id,
                    'consequent_id' => $items[$j]->id,
                    'support' => ($total / $transactions->count()) * 100,
                    'confidence' => ($total / $t1) * 100,
                ]);
                $recommendation->save();

                $recommendation2 = new Recommendation();
                $recommendation2->fill([
                    'antecedent_id' => $items[$j]->id,
                    'consequent_id' => $items[$i]->id,
                    'support' => ($total / $transactions->count()) * 100,
                    'confidence' => ($total / $t2) * 100,
                ]);
                $recommendation2->save();
            }
        }
    }
}

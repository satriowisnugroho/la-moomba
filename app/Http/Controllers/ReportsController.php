<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ReportsController extends Controller
{
    public function index(Request $request)
    {
        $type = $request->get('type') ?: 'day';

        if ($type == 'month') {
            $reports = Order::select(
                DB::raw('count(id) as `count`'),
                DB::raw("DATE_FORMAT(created_at, '%m-%Y') date"),
                DB::raw("MONTH(created_at) as month, YEAR(created_at) as year"),
                DB::raw('sum(price) as total_price')
            )
                ->where('status', 2)
                ->groupby('date', 'month', 'year')
                ->orderBy('year', 'desc')
                ->orderBy('month', 'desc')
                ->get();
        } else {
            $reports = Order::select(
                DB::raw('count(id) as `count`'),
                DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y') date"),
                DB::raw("DAY(created_at) as day, MONTH(created_at) as month, YEAR(created_at) as year"),
                DB::raw('sum(price) as total_price')
            )
                ->where('status', 2)
                ->groupby('date', 'day', 'month', 'year')
                ->orderBy('year', 'desc')
                ->orderBy('month', 'desc')
                ->orderBy('day', 'desc')
                ->get();
        }

        return view('admin.reports.index', compact('reports', 'type'));
    }

    public function detail(Request $request)
    {
        $day = $request->get('day');
        $year = $request->get('year');
        $month = $request->get('month');

        $orders = Order::where('status', 2)
            ->whereYear('created_at', '=', $year)
            ->whereMonth('created_at', '=', $month);

        if ($day) {
            $orders = $orders->whereDay('created_at', '=', $day);
        }

        $orders = $orders->paginate();

        return view('admin.reports.detail', compact('orders', 'day', 'month', 'year'));
    }
}

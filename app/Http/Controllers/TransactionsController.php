<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;

class TransactionsController extends Controller
{
    public function index(Request $request)
    {
        $q = $request->get('q');

        $orders = Order::where('status', 2)->orderBy('updated_at', 'desc');

        if ($q) {
            $orders = $orders->where('name', 'like', "%$q%");
        }

        $orders = $orders->paginate();

        return view('admin.transactions.index', compact('orders'));
    }

    public function show(Request $request, Order $order)
    {
        if ($order->status != 2) {
            abort(404);
        }

        $day = $request->get('day');
        $year = $request->get('year');
        $month = $request->get('month');

        $totalPrice = 0;
        foreach ($order->menus as $menu) {
            $totalPrice += $menu->price * $menu->pivot->qty;
        }

        return view('admin.transactions.show', compact('order', 'totalPrice', 'day', 'month', 'year'));
    }
}

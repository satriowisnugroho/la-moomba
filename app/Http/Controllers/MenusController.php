<?php

namespace App\Http\Controllers;

use App\Http\Requests\MenuRequest;
use App\Menu;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;

class MenusController extends Controller
{
    public function index(Request $request)
    {
        $q = $request->get('q');
        $menus = Menu::orderBy('created_at', 'desc');

        if ($q) {
            $menus = $menus->where('name', 'like', "%$q%");
        }

        $menus = $menus->paginate();

        return view('admin.menus.index', compact('menus'));
    }

    public function show(Menu $menu)
    {
        return view('admin.menus.show', compact('menu'));
    }

    public function create()
    {
        $categories = config('moomba')['categories'];
        return view('admin.menus.create', compact('categories'));
    }

    public function edit(Menu $menu)
    {
        $categories = config('moomba')['categories'];
        $category = $categories[$menu->category];
        return view('admin.menus.edit', compact('menu', 'categories', 'category'));
    }

    public function store(MenuRequest $request)
    {
        $image = $request->file('image');
        $imageName = $request->name . '-' . Carbon::now()->timestamp . '.' . $image->getClientOriginalExtension();
        $destinationPath = public_path('/images/menus');

        $bannerImageName = null;

        if ($request->hasFile('banner_image')) {
            $bannerImage = $request->file('banner_image');
            $bannerImageName = 'banner-' . $request->name . '-' . Carbon::now()->timestamp . '.' . $bannerImage->getClientOriginalExtension();

            if (!$bannerImage->move($destinationPath, $bannerImageName)) {
                flash()->error('Gagal menambah menu baru');
                return redirect('admin/menus');
            }
        }

        if ($image->move($destinationPath, $imageName)) {
            $menu = new Menu();
            $menu->fill(array_merge($request->all(), ['image' => $imageName, 'banner_image' => $bannerImageName]))->save();
        } else {
            flash()->error('Gagal menambah menu baru');
            return redirect('admin/menus');
        }

        flash()->success('Berhasil menambah menu baru');
        return redirect('admin/menus');
    }

    public function getCategories($category)
    {
        return config('moomba')['categories'][$category];
    }

    public function update(MenuRequest $request, Menu $menu)
    {
        $data = $request->all();
        $destinationPath = public_path('/images/menus');

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $imageName = $request->name . '-' . Carbon::now()->timestamp . '.' . $image->getClientOriginalExtension();
            if (!$image->move($destinationPath, $imageName)) {
                flash()->error('Gagal mengubah menu');
                return redirect('admin/menus');
            }
            File::delete(public_path('/images/menus') . '/' . $menu->image);
            $data = array_merge($data, ['image' => $imageName]);
        }

        if ($request->hasFile('banner_image')) {
            $bannerImage = $request->file('banner_image');
            $bannerImageName = 'banner-' . $request->name . '-' . Carbon::now()->timestamp . '.' . $bannerImage->getClientOriginalExtension();

            if (!$bannerImage->move($destinationPath, $bannerImageName)) {
                flash()->error('Gagal mengubah menu');
                return redirect('admin/menus');
            }
            File::delete(public_path('/images/menus') . '/' . $menu->banner_image);
            $data = array_merge($data, ['banner_image' => $bannerImageName]);
        }

        $menu->fill($data)->save();

        flash()->success('Berhasil mengubah menu');
        return redirect('admin/menus');
    }

    public function updateStock(Menu $menu)
    {
        $menu->is_out = !$menu->is_out;
        $menu->save();
        return redirect()->back();
    }

    public function specialToday(Menu $menu)
    {
        $menu->is_special_today = !$menu->is_special_today;
        $menu->save();
        return redirect()->back();
    }

    public function destroy(Menu $menu)
    {
        File::delete(public_path('/images/menus') . '/' . $menu->image);
        if ($menu->banner_image) {
            File::delete(public_path('/images/menus') . '/' . $menu->banner_image);
        }
        $menu->delete();
        flash()->success('Berhasil menghapus menu');
        return redirect('admin/menus');
    }
}

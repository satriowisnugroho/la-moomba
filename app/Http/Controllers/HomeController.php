<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\Menu;
use App\Order;
use App\Recommendation;
use Illuminate\Http\Request;
use Pusher\Pusher;
use PDF;

class HomeController extends Controller
{
    public function home()
    {
        $menus = Menu::orderBy('count_sold', 'desc')->where('is_out', false)->take(4)->get();

        $banners = Menu::where('is_out', false)
            ->where('is_special_today', true)
            ->whereNotNull('banner_image')
            ->get();

        return view('index', compact('menus', 'banners'));
    }

    public function index($category, $type = null)
    {
        $categoryKey = $category;
        $typeKey = $type;

        $isExist = array_key_exists($category, config('moomba')['categories']);

        if (!$isExist) {
            return abort(404);
        }

        $category = config('moomba')['categories'][$categoryKey];
        $isExist = $type ? array_key_exists($typeKey, $category['types']) : true;

        if (!$isExist) {
            return abort(404);
        }

        $type = $category['types'][$typeKey];
        $category = $category['title'];

        $menuPagination = Menu::getByCategoryAndType($categoryKey, $typeKey)->where('is_out', false)->paginate();
        $menus = $menuPagination->chunk(4);

        return view('menus.index', compact('menuPagination', 'menus', 'categoryKey', 'category', 'type'));
    }

    public function specials()
    {
        $menuPagination = Menu::where('is_out', false)->where('is_special_today', true)->paginate();
        $menus = $menuPagination->chunk(4);

        return view('menus.specials', compact('menuPagination', 'menus'));
    }

    public function show(Menu $menu)
    {
        $recommendations = $menu->getRecommendations()->chunk(4);

        return view('menus.show', compact('menu', 'recommendations'));
    }

    public function checkout()
    {
        return view('checkout');
    }

    public function payment(OrderRequest $request)
    {
        $menuIds = explode(',', $request->get('menu_ids'));
        $quantities = explode(',', $request->get('quantities'));
        $notes = explode(',', $request->get('notes'));

        $price = 0;

        foreach ($menuIds as $key => $id) {
            $menu = Menu::find($id);
            $price += $menu->price * $quantities[$key];
        }

        $order = new Order();
        $order->name = $request->get('name');
        $order->table_number = $request->get('table_number');
        $order->price = $price;
        $order->save();

        foreach ($menuIds as $key => $id) {
            $menu = Menu::find($id);
            $menu->orders()->attach([
                $order->id => [
                    'qty' => $quantities[$key],
                    'note' => $notes[$key],
                    'price' => $menu->price,
                    'total_price' => $menu->price * $quantities[$key],
                ]
            ]);
            $menu->count_sold += $quantities[$key];
            $menu->save();
        }

        $pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            ['encrypted' => true]
        );

        $data['message'] = 'New order';
        $pusher->trigger('my-channel', 'my-event', $data);

        session(['order_id' => $order->id]);

        return redirect('/');
    }

    public function search(Request $request)
    {
        $q = $request->get('q');
        $menuPagination = Menu::where('name', 'like', "%$q%")->paginate();
        $menus = $menuPagination->chunk(4);

        return view('menus.search', compact('menuPagination', 'menus'));
    }

    public function dashboard()
    {
        $countMenu = Menu::count();
        $countOrder = Order::whereNotIn('status', [2])->count();
        $countTransaction = Order::where('status', 2)->count();
        $countRecommendation = Recommendation::count();

        return view('admin.dashboard', compact(
            'countMenu',
            'countOrder',
            'countTransaction',
            'countRecommendation'
        ));
    }

    public function order()
    {
        $orderId = session('order_id');

        if (!$orderId) {
            return redirect('/');
        }

        $order = Order::find($orderId);

        return view('order', compact('order'));
    }

    public function printInvoice(Request $request)
    {
        $orderId = session('order_id');

        if (!$orderId) {
            abort(404);
        }

        $order = Order::find($orderId);

        if ($order->status != 2) {
            flash()->warning('Status pesanan belum selesai, anda tidak dapat mencetak invoice');

            return redirect('order');
        }

        $request->session()->forget('order_id');

        $pdf = PDF::loadView('invoice', compact('order'));
        return $pdf->download('invoice.pdf');
    }
}

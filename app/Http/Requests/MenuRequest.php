<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MenuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $image = 'required|image|mimes:JPEG,JPG,jpg,jpeg,png|max:2048';

        if ($this->isMethod('put')) {
            $image = 'nullable|image|mimes:JPEG,JPG,jpg,jpeg,png|max:2048';
        }

        return [
            'name' => 'required',
            'price' => 'required',
            'category' => 'required',
            'type' => 'nullable',
            'description' => 'required',
            'image' => $image,
            'banner_content' => 'nullable',
            'banner_image' => 'nullable|image|mimes:JPEG,JPG,jpg,jpeg,png|max:2048',
        ];
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recommendation extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'antecedent_id',
        'consequent_id',
        'support',
        'confidence',
    ];

    public function antecedent()
    {
        return $this->belongsTo(Menu::class, 'antecedent_id');
    }

    public function consequent()
    {
        return $this->belongsTo(Menu::class, 'consequent_id');
    }
}

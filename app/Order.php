<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['status'];

    public function menus()
    {
        return $this->belongsToMany(Menu::class)->withPivot('qty', 'note', 'is_done', 'price', 'total_price');
    }

    public function getStatus()
    {
        return config('moomba.status')[$this->status];
    }
}

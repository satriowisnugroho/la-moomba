<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'price',
        'category',
        'type',
        'description',
        'image',
        'banner_content',
        'banner_image',
    ];

    public function getCategory()
    {
        $categories = config('moomba.categories');
        $category = new \stdClass();
        $category->title = $categories[$this->category]['title'];
        $category->type = $categories[$this->category]['types'][$this->type];
        return $category;
    }

    public function orders()
    {
        return $this->belongsToMany(Order::class)->withPivot('qty', 'note', 'is_done', 'price', 'total_price');
    }

    public function antecedents()
    {
        return $this->hasMany(Recommendation::class, 'antecedent_id');
    }

    public function getRecommendations()
    {
        return $this->antecedents()->orderBy('confidence', 'desc')->get();
    }

    public function scopeGetByCategoryAndType($query, $category, $type)
    {
        return $query->where('category', $category)->where('type', $type);
    }
}

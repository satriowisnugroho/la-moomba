@extends('layouts.main')

@section('breadcrumb', $menu->getCategory()->title)
@section('breadcrumb2', $menu->getCategory()->type)

@section('content')
  <!-- banner -->
  <div class="banner">
    @include('layouts.sidebar')
    <div class="w3l_banner_nav_right">
      <div class="agileinfo_single" style="border-top: 1px solid #B58F62">
        <h5>{{ $menu->name }}</h5>
        <div class="col-md-4 agileinfo_single_left text-center">
          <img id="example" src="{{ asset('images/menus/' . $menu->image) }}" alt=" " class="img-responsive" />
        </div>
        <div class="col-md-8 agileinfo_single_right">
          <div class="w3agile_description">
            <h4>Description :</h4>
            <p>{{ $menu->description }}</p>
          </div>
          <div class="snipcart-item block">
            <div class="snipcart-thumb agileinfo_single_right_snipcart">
              <h4>{{ price($menu->price) }}</h4>
            </div>
            <div class="snipcart-details agileinfo_single_right_details">
              <form action="#" method="post">
                <fieldset>
                  <input type="hidden" name="cmd" value="_cart" />
                  <input type="hidden" name="add" value="1" />
                  <input type="hidden" name="business" value=" " />
                  <input type="hidden" name="item_name" value="{{ $menu->name }}" />
                  <input type="hidden" name="amount" value="{{ $menu->price }}" />
                  <input type="hidden" name="currency_code" value="IDR" />
                  <input type="hidden" name="return" value=" " />
                  <input type="hidden" name="cancel_return" value=" " />
                  <input type="submit" name="submit" value="Add to cart" class="button" />
                </fieldset>
              </form>
            </div>
          </div>
        </div>
        <div class="clearfix"> </div>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
  <!-- banner -->
  <div class="w3ls_w3l_banner_nav_right_grid w3ls_w3l_banner_nav_right_grid_popular">
    <div class="container">
      <h3>Recommendation</h3>
      @foreach($recommendations as $rec)
      <div class="w3ls_w3l_banner_nav_right_grid1">
        @foreach($rec as $recommendation)
          <div class="col-md-3 w3ls_w3l_banner_left">
            <div class="hover14 column">
              <div class="agile_top_brand_left_grid w3l_agile_top_brand_left_grid">
                <div class="agile_top_brand_left_grid_pos">
                  <img src="{{ asset('images/offer.png') }}" alt=" " class="img-responsive" />
                </div>
                <div class="agile_top_brand_left_grid1">
                  <figure>
                    <div class="snipcart-item block">
                      <div class="snipcart-thumb">
                        <a href="{{ url('menus/' . $recommendation->consequent->id) }}"><img src="{{ asset('images/menus/' . $recommendation->consequent->image) }}" alt=" " class="img-responsive" /></a>
                        <p>{{ $recommendation->consequent->name }}</p>
                        <h4>{{ price($recommendation->consequent->price) }}</h4>
                      </div>
                      <div class="snipcart-details">
                        <form action="#" method="post">
                          <fieldset>
                            <input type="hidden" name="cmd" value="_cart" />
                            <input type="hidden" name="add" value="1" />
                            <input type="hidden" name="business" value=" " />
                            <input type="hidden" name="menu_id" value="{{ $recommendation->consequent->id }}" />
                            <input type="hidden" name="item_name" value="{{ $recommendation->consequent->name }}" />
                            <input type="hidden" name="amount" value="{{ $recommendation->consequent->price }}" />
                            <input type="hidden" name="href" value="{{ url('menus/' . $recommendation->consequent->id) }}" />
                            <input type="hidden" name="image" value="{{ asset('images/menus/' . $recommendation->consequent->image) }}" />
                            <input type="hidden" name="currency_code" value="IDR" />
                            <input type="hidden" name="return" value=" " />
                            <input type="hidden" name="cancel_return" value=" " />
                            <input type="submit" name="submit" value="Add to cart" class="button" />
                          </fieldset>
                        </form>
                      </div>
                    </div>
                  </figure>
                </div>
              </div>
            </div>
          </div>
        @endforeach
        <div class="clearfix"> </div>
      </div>
      @endforeach
    </div>
  </div>
  <!-- //brands -->
@endsection

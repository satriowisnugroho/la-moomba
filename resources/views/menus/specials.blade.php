@extends('layouts.main')

@section('content')
  <!-- banner -->
  <div class="banner">
    @include('layouts.sidebar')
    <div class="w3l_banner_nav_right">
      <div class="w3ls_w3l_banner_nav_right_grid w3ls_w3l_banner_nav_right_grid_veg" style="border-top: 1px solid #B58F62; padding-top: 50px">
        <h3 class="w3l_fruit">Today's Special Offers</h3>

        @foreach($menus as $key => $menuParents)
          <div class="w3ls_w3l_banner_nav_right_grid1 w3ls_w3l_banner_nav_right_grid1_veg">
            @foreach($menuParents as $menu)
              <div class="col-md-3 w3ls_w3l_banner_left">
                <div class="hover14 column">
                  <div class="agile_top_brand_left_grid w3l_agile_top_brand_left_grid">
                    <div class="agile_top_brand_left_grid1">
                      <figure>
                        <div class="snipcart-item block">
                          <div class="snipcart-thumb">
                            <a href="{{ url('menus/' . $menu->id . '/detail') }}"><img src="{{ asset('images/menus/' . $menu->image) }}" alt=" " class="img-responsive" /></a>
                            <p>{{ $menu->name }}</p>
                            <h4>{{ price($menu->price) }}</h4>
                          </div>
                          <div class="snipcart-details">
                            <form action="#" method="post">
                              <fieldset>
                                <input type="hidden" name="cmd" value="_cart" />
                                <input type="hidden" name="add" value="1" />
                                <input type="hidden" name="business" value=" " />
                                <input type="hidden" name="menu_id" value="{{ $menu->id }}" />
                                <input type="hidden" name="item_name" value="{{ $menu->name }}" />
                                <input type="hidden" name="amount" value="{{ $menu->price }}" />
                                <input type="hidden" name="href" value="{{ url('menus/' . $menu->id) }}" />
                                <input type="hidden" name="image" value="{{ asset('images/menus/' . $menu->image) }}" />
                                <input type="hidden" name="currency_code" value="IDR" />
                                <input type="hidden" name="return" value=" " />
                                <input type="hidden" name="cancel_return" value=" " />
                                <input type="submit" name="submit" value="Add to cart" class="button" />
                              </fieldset>
                            </form>
                          </div>
                        </div>
                      </figure>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
            <div class="clearfix"> </div>
          </div>
        @endforeach

        <div class="text-center">
          {!! $menuPagination->links() !!}
        </div>

      </div>
    </div>
    <div class="clearfix"></div>
  </div>
  <!-- banner -->
@endsection

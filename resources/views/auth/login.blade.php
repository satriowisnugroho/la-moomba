@extends('layouts.admin-main')

@section('content')
  <!-- banner -->
  <div class="banner">
    <div class="w3ls_w3l_banner_nav_right_grid w3ls_w3l_banner_nav_right_grid_veg"
         style="border-top: 1px solid #B58F62; padding-top: 50px">
      <h3 class="w3l_fruit">Login Panel</h3>
      <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default panel-auth">
          <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
              {{ csrf_field() }}

              <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                <label for="username" class="col-md-4 control-label">Username</label>

                <div class="col-md-6">
                  <input id="username" type="username" class="form-control" name="username"
                         value="{{ old('username') }}" required autofocus>

                  @if ($errors->has('username'))
                    <span class="help-block">
                  <strong>{{ $errors->first('username') }}</strong>
                </span>
                  @endif
                </div>
              </div>

              <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                <label for="password" class="col-md-4 control-label">Password</label>

                <div class="col-md-6">
                  <input id="password" type="password" class="form-control" name="password" required>

                  @if ($errors->has('password'))
                    <span class="help-block">
                  <strong>{{ $errors->first('password') }}</strong>
                </span>
                  @endif
                </div>
              </div>

              <div class="form-group">
                <div class="col-md-6 col-md-offset-4">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                    </label>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="col-md-8 col-md-offset-4">
                  <button type="submit" class="btn btn-primary">
                    Login
                  </button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
  <!-- banner -->
@endsection

@push('scripts')

<script>
  $('#category').change(function () {
    $.get('{{ url('categories') }}/' + $(this).val(), function (res) {
      if (res.types) {
        $('#type-form').removeClass('hide');
        $('#type').html('');
        for (var key in res.types) {
          // skip loop if the property is from prototype
          if (!res.types.hasOwnProperty(key)) continue;
          var val = res.types[key];
          $('#type').append('<option value="' + key + '">' + val + '</option>');
        }
      } else {
        $('#type-form').addClass('hide');
      }
    });
  });
</script>

@endpush

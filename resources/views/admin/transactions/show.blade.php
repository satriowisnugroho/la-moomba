@extends('layouts.admin-main')

@section('breadcrumb')
  <li>Orders <span>|</span></li>
  <li>Detail</li>
@endsection

@section('content')
  <!-- banner -->
  <div class="banner">
    @include('layouts.admin-sidebar')
    <div class="w3l_banner_nav_right">
      <div class="w3ls_w3l_banner_nav_right_grid w3ls_w3l_banner_nav_right_grid_veg" style="border-top: 1px solid #B58F62; padding-top: 50px">
        <h3 class="w3l_fruit">Table Number {{ $order->table_number }}</h3>
        <div class="col-md-10 col-md-offset-1">
          @if($year)
            <a href="{{ url("/admin/reports/detail?year=$year&month=$month") }}{{ $day ? "&day=$day" : '' }}" class="btn btn-default btn-back">
              <i class="fa fa-chevron-left"></i> Back
            </a>
          @else
            <a href="{{ url('/admin/transactions') }}" class="btn btn-default btn-back">
              <i class="fa fa-chevron-left"></i> Back
            </a>
          @endif
          <table class="table table-bordered">
            <thead>
            <tr>
              <th class="text-center">No</th>
              <th>Menu</th>
              <th>Note</th>
              <th class="text-center">Qty</th>
              <th>Price</th>
              <th>Sub Total</th>
            </tr>
            </thead>
            <tbody>
            @foreach($order->menus as $key => $menu)
              <tr>
                <td class="text-center">{{ $key + 1 }}</td>
                <td>{{ $menu->name }}</td>
                <td>{{ $menu->pivot->note }}</td>
                <td class="text-center">{{ $menu->pivot->qty }}</td>
                <td>{{ price($menu->price) }}</td>
                <td>{{ price($menu->price * $menu->pivot->qty) }}</td>
              </tr>
            @endforeach
            <tr>
              <td colspan="5" class="text-right">Total Price</td>
              <td>{{ price($totalPrice) }}</td>
            </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
  <!-- banner -->
@endsection

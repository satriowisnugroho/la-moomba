@extends('layouts.admin-main')

@section('breadcrumb')
  <li>Dashboard</li>
@endsection

@section('content')
  <!-- banner -->
  <div class="banner">
    @include('layouts.admin-sidebar')
    <div class="w3l_banner_nav_right">
      <div class="w3ls_w3l_banner_nav_right_grid w3ls_w3l_banner_nav_right_grid_veg" style="border-top: 1px solid #B58F62; padding-top: 50px">
        <h3 class="w3l_fruit">Dashboard</h3>
        <div class="row fix-row">
          <div class="col-md-3 dashboard-item">
            <a href="{{ url('admin/menus') }}">
              <div class="dashboard-sub">
                <h1>{{ $countMenu }}</h1>
                <p>Menu</p>
              </div>
            </a>
          </div>
          <div class="col-md-3 dashboard-item">
            <a href="{{ url('admin/orders') }}">
              <div class="dashboard-sub">
                <h1>{{ $countOrder }}</h1>
                <p>Order</p>
              </div>
            </a>
          </div>
          <div class="col-md-3 dashboard-item">
            <a href="{{ url('admin/transactions') }}">
              <div class="dashboard-sub">
                <h1>{{ $countTransaction }}</h1>
                <p>Transaction</p>
              </div>
            </a>
          </div>
          <div class="col-md-3 dashboard-item">
            <a href="{{ url('admin/recommendations') }}">
              <div class="dashboard-sub">
                <h1>{{ $countRecommendation }}</h1>
                <p>Recommendation</p>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
  <!-- banner -->
@endsection

@extends('layouts.admin-main')

@section('breadcrumb')
  <li>Menus</li>
@endsection

@section('content')
  <!-- banner -->
  <div class="banner">
    @include('layouts.admin-sidebar')
    <div class="w3l_banner_nav_right">
      <div class="w3ls_w3l_banner_nav_right_grid w3ls_w3l_banner_nav_right_grid_veg" style="border-top: 1px solid #B58F62; padding-top: 50px">
        <h3 class="w3l_fruit">Menus</h3>
        <div class="col-md-12">
          <div class="row">
            <div class="col-md-6">
              <a href="{{ url('/admin/menus/create') }}" class="btn btn-info"><span class="fa fa-plus"></span> Add Menu</a>
            </div>
            <div class="col-md-3 col-md-offset-3">
              <form>
                <div class="input-group">
                  <input type="text" class="form-control" name="q" placeholder="Search">
                  <span class="input-group-btn">
                  <button class="btn btn-default" type="submit">
                    <i class="fa fa-search"></i>
                  </button>
                </span>
                </div>
              </form>
            </div>
          </div>
          @include('flash::message')
          <table class="table table-bordered">
            <thead>
            <tr>
              <th>No</th>
              <th>Name</th>
              <th>Price</th>
              <th>Category</th>
              <th>Type</th>
              <th>Status</th>
              <th class="text-center">#</th>
            </tr>
            </thead>
            <tbody>
            @foreach($menus as $key => $menu)
              <tr>
                <td class="text-center">{{ ($menus->currentPage() - 1) * $menus->perPage() + ($key + 1) }}</td>
                <td>{{ $menu->name }}</td>
                <td>{{ price($menu->price) }}</td>
                <td>{{ $menu->getCategory()->title }}</td>
                <td>{{ $menu->getCategory()->type }}</td>
                <td>{{ $menu->is_out ? 'Out of Stock' : 'Available' }}</td>
                <td class="text-center">
                  <form action="{{ url('/admin/menus/' . $menu->id) }}" id="delete-form" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <a href="{{ url('/admin/menus/' . $menu->id) . '/special' }}" class="btn btn-default">
                      <i class="fa {{ $menu->is_special_today ? 'fa-star' : 'fa-star-o' }}"></i>
                    </a>
                    <a href="{{ url('/admin/menus/' . $menu->id) }}" class="btn btn-default">Show</a>
                    <a href="{{ url('/admin/menus/' . $menu->id . '/edit') }}" class="btn btn-success">Edit</a>
                    <button type="button" class="btn btn-danger btn-delete">Delete</button>
                  </form>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
          <div class="text-center">
            {!! $menus->links() !!}
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
  <!-- banner -->
@endsection

@push('scripts')

<script>
  $('.btn-delete').click(function () {
    var res = confirm('Apakah anda ingin menghapusnya?');
    if (res) {
      $(this).parents().submit();
    }
  });
</script>

@endpush

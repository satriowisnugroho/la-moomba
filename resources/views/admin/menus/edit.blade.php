@extends('layouts.admin-main')

@section('breadcrumb')
  <li>Menus <span>|</span></li>
  <li>Edit</li>
@endsection

@section('content')
  <!-- banner -->
  <div class="banner">
    @include('layouts.admin-sidebar')
    <div class="w3l_banner_nav_right">
      <div class="w3ls_w3l_banner_nav_right_grid w3ls_w3l_banner_nav_right_grid_veg" style="border-top: 1px solid #B58F62; padding-top: 50px">
        <h3 class="w3l_fruit">Edit Menu</h3>
        <div class="col-md-10 col-md-offset-1">
          <form action="{{ url('/admin/menus/' . $menu->id) }}" method="POST" enctype="multipart/form-data">
            {{ csrf_field() }}
            {{ method_field('PUT') }}

            <div class="row">
              <div class="col-md-6">
                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                  <label>Name</label>
                  <input type="text" class="form-control" name="name" value="{{ old('name', $menu->name) }}">
                  @if ($errors->has('name'))
                    <span class="help-block">
                  <strong>{{ $errors->first('name') }}</strong>
                </span>
                  @endif
                </div>

                <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                  <label>Price</label>
                  <input type="number" class="form-control" name="price" value="{{ old('price', $menu->price) }}">
                  @if ($errors->has('price'))
                    <span class="help-block">
                  <strong>{{ $errors->first('price') }}</strong>
                </span>
                  @endif
                </div>

                <div class="form-group{{ $errors->has('category') ? ' has-error' : '' }}">
                  <label>Category</label>
                  <select name="category" id="category" class="form-control">
                    @foreach($categories as $key => $category)
                      <option value="{{ $key }}"{{ $key === $menu->category ? ' selected' : '' }}>{{ $category['title'] }}</option>
                    @endforeach
                  </select>
                  @if ($errors->has('category'))
                    <span class="help-block">
                  <strong>{{ $errors->first('category') }}</strong>
                </span>
                  @endif
                </div>

                <div class="form-group{{ $errors->has('type') ? ' has-error' : '' }}{{ $category['types'] == null ? '' : ' hide' }}" id="type-form">
                  <label>Type</label>
                  <select name="type" id="type" class="form-control">
                    @if($category['types'] == null)
                      @foreach($category['types'] as $key => $type)
                        <option value="{{ $key }}"{{ $key === $menu->type ? ' selected' : '' }}>{{ $type }}</option>
                      @endforeach
                    @endif
                  </select>
                  @if ($errors->has('type'))
                    <span class="help-block">
                  <strong>{{ $errors->first('type') }}</strong>
                </span>
                  @endif
                </div>

                <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                  <label>Description</label>
                  <textarea name="description" class="form-control" cols="30" rows="10">{{ old('description', $menu->description) }}</textarea>
                  @if ($errors->has('description'))
                    <span class="help-block">
                  <strong>{{ $errors->first('description') }}</strong>
                </span>
                  @endif
                </div>

                <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
                  <label>Image</label>
                  <input type="file" name="image" class="form-control">
                  @if ($errors->has('image'))
                    <span class="help-block">
                  <strong>{{ $errors->first('image') }}</strong>
                </span>
                  @endif
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group{{ $errors->has('banner_content') ? ' has-error' : '' }}">
                  <label>Banner Content</label>
                  <input type="text" name="banner_content" class="form-control" value="{{ old('banner_content', $menu->banner_content) }}">
                  @if ($errors->has('banner_content'))
                    <span class="help-block">
                  <strong>{{ $errors->first('banner_content') }}</strong>
                </span>
                  @endif
                </div>

                <div class="form-group{{ $errors->has('banner_image') ? ' has-error' : '' }}">
                  <label>Banner Image</label>
                  <input type="file" name="banner_image" class="form-control"">
                  @if ($errors->has('banner_image'))
                    <span class="help-block">
                  <strong>{{ $errors->first('banner_image') }}</strong>
                </span>
                  @endif
                </div>

                <button class="btn btn-success" type="submit">Update</button>
                <a href="{{ url('/admin/menus') }}" class="btn btn-default" type="submit">Cancel</a>
              </div>
            </div>
          </form>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
  <!-- banner -->
@endsection

@push('scripts')

<script>
  $('#category').change(function () {
    $.get('{{ url('categories') }}/' + $(this).val(), function (res) {
      if (res.types) {
        $('#type-form').removeClass('hide');
        $('#type').html('');
        for (var key in res.types) {
          // skip loop if the property is from prototype
          if (!res.types.hasOwnProperty(key)) continue;
          var val = res.types[key];
          $('#type').append('<option value="' + key + '">' + val + '</option>');
        }
      } else {
        $('#type-form').addClass('hide');
        $('#type').val(null);
      }
    });
  });
</script>

@endpush

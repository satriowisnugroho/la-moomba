@extends('layouts.admin-main')

@section('breadcrumb')
  <li>Menus <span>|</span></li>
  <li>Detail</li>
@endsection

@section('content')
  <!-- banner -->
  <div class="banner">
    @include('layouts.admin-sidebar')
    <div class="w3l_banner_nav_right">
      <div class="w3ls_w3l_banner_nav_right_grid w3ls_w3l_banner_nav_right_grid_veg" style="border-top: 1px solid #B58F62; padding-top: 50px">
        <h3 class="w3l_fruit">Detail Menu</h3>
        <div class="col-md-2 col-md-offset-2">
          <div class="form-group">
            <label>Image</label>
            <img src="{{ asset('images/menus/' . $menu->image) }}" width="100%">
          </div>
          @if ($menu->banner_image)
          <div class="form-group">
            <label>Banner Image</label>
            <img src="{{ asset('images/menus/' . $menu->banner_image) }}" width="100%">
          </div>
          @endif
        </div>
        <div class="col-md-6">
          <div class="form-group">
            <label>Name</label>
            <p>{{ $menu->name }}</p>
          </div>
          <div class="form-group">
            <label>Price</label>
            <p>{{ price($menu->price) }}</p>
          </div>
          <div class="form-group">
            <label>Category</label>
            <p>{{ $menu->getCategory()->title }}</p>
          </div>
          <div class="form-group">
            <label>Type</label>
            <p>{{ $menu->getCategory()->type }}</p>
          </div>
          <div class="form-group">
            <label>Status</label>
            <p>{{ $menu->is_out ? 'Out of Stock' : 'available' }}</p>
          </div>
          <div class="form-group">
            <label>Description</label>
            <p>{{ $menu->description }}</p>
          </div>
          <div class="form-group">
            <label>Banner Content</label>
            <p>{{ $menu->banner_content ?: '-' }}</p>
          </div>
          @if($menu->is_out)
            <a href="{{ url('/admin/menus/' . $menu->id . '/stock') }}" class="btn btn-info">Available</a>
          @else
            <a href="{{ url('/admin/menus/' . $menu->id . '/stock') }}" class="btn btn-danger">Out of Stock</a>
          @endif
          <a href="{{ url('/admin/menus') }}" class="btn btn-default">Back</a>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
  <!-- banner -->
@endsection

@extends('layouts.admin-main')

@section('breadcrumb')
  <li>Orders <span>|</span></li>
  <li>Menus</li>
@endsection

@section('content')
  <!-- banner -->
  <div class="banner">
    @include('layouts.admin-sidebar')
    <div class="w3l_banner_nav_right">
      <div class="w3ls_w3l_banner_nav_right_grid w3ls_w3l_banner_nav_right_grid_veg" style="border-top: 1px solid #B58F62; padding-top: 50px">
        <h3 class="w3l_fruit">Add Menu</h3>
        <div class="col-md-8 col-md-offset-2">
          <div class="row">
            <div class="col-md-6">
              <a href="{{ url("/admin/orders/$order->id") }}" class="btn btn-default">
                <i class="fa fa-chevron-left"></i> Back
              </a>
            </div>
            <div class="col-md-4 col-md-offset-2">
              <form>
                <div class="input-group">
                  <input type="text" class="form-control" name="q" placeholder="Search">
                  <span class="input-group-btn">
                  <button class="btn btn-default" type="submit">
                    <i class="fa fa-search"></i>
                  </button>
                </span>
                </div>
              </form>
            </div>
          </div>
          @include('flash::message')
          <table class="table table-bordered">
            <thead>
            <tr>
              <th>No</th>
              <th>Name</th>
              <th>Price</th>
              <th>Category</th>
              <th>Type</th>
              <th class="text-center">#</th>
            </tr>
            </thead>
            <tbody>
            @foreach($menus as $key => $menu)
              <tr>
                <td class="text-center">{{ ($menus->currentPage() - 1) * $menus->perPage() + ($key + 1) }}</td>
                <td>{{ $menu->name }}</td>
                <td>{{ price($menu->price) }}</td>
                <td>{{ $menu->getCategory()->title }}</td>
                <td>{{ $menu->getCategory()->type }}</td>
                <td class="text-center">
                  <a href="{{ url("/admin/orders/$order->id/menus/$menu->id") }}" class="btn btn-success">Add</a>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
          <div class="text-center">
            {!! $menus->links() !!}
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
  <!-- banner -->
@endsection

@push('scripts')

<script>
  $('.btn-delete').click(function () {
    var res = confirm('Apakah anda ingin menghapusnya?');
    if (res) {
      $(this).parents().submit();
    }
  });
</script>

@endpush

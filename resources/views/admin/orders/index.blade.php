@extends('layouts.admin-main')

@section('breadcrumb')
  <li>Orders</li>
@endsection

@section('content')
  <!-- banner -->
  <div class="banner">
    @include('layouts.admin-sidebar')
    <div class="w3l_banner_nav_right">
      <div class="w3ls_w3l_banner_nav_right_grid w3ls_w3l_banner_nav_right_grid_veg" style="border-top: 1px solid #B58F62; padding-top: 50px">
        <h3 class="w3l_fruit">Orders</h3>
        <div class="col-md-8 col-md-offset-2">
          <div class="row">
            <div class="col-md-4 col-md-offset-8">
              <form>
                <div class="input-group">
                  <input type="text" class="form-control" name="q" placeholder="Search">
                  <span class="input-group-btn">
                  <button class="btn btn-default" type="submit">
                    <i class="fa fa-search"></i>
                  </button>
                </span>
                </div>
              </form>
            </div>
          </div>
          @include('flash::message')
          <table class="table table-bordered">
            <thead>
            <tr>
              <th>No</th>
              <th>Table Number</th>
              <th>Name</th>
              <th>Status</th>
              <th class="text-center">#</th>
            </tr>
            </thead>
            <tbody>
            @forelse($orders as $key => $order)
              <tr>
                <td class="text-center">{{ ($orders->currentPage() - 1) * $orders->perPage() + ($key + 1) }}</td>
                <td class="text-center">{{ $order->table_number }}</td>
                <td>{{ $order->name }}</td>
                <td>{{ $order->getStatus() }}</td>
                <td class="text-center">
                  <a href="{{ url('/admin/orders/' . $order->id) }}" class="btn btn-default">Show</a>
                  <a href="{{ url('/admin/orders/' . $order->id . '/process') }}" class="btn btn-info">Process</a>
                  <a href="{{ url('/admin/orders/' . $order->id . '/done') }}" class="btn btn-success">Done</a>
                </td>
              </tr>
            @empty
              <tr>
                <td class="text-center" colspan="5">Tidak ada pesanan</td>
              </tr>
            @endforelse
            </tbody>
          </table>
          <div class="text-center">
            {!! $orders->links() !!}
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
  <!-- banner -->
@endsection

@push('scripts')

<script>
  $('.new-order').addClass('hide');
  $.removeCookie('new-order');
</script>

@endpush

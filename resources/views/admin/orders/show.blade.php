@extends('layouts.admin-main')

@section('breadcrumb')
  <li>Orders <span>|</span></li>
  <li>Detail</li>
@endsection

@section('content')
  <!-- banner -->
  <div class="banner">
    @include('layouts.admin-sidebar')
    <div class="w3l_banner_nav_right">
      <div class="w3ls_w3l_banner_nav_right_grid w3ls_w3l_banner_nav_right_grid_veg" style="border-top: 1px solid #B58F62; padding-top: 50px">
        <h3 class="w3l_fruit">Table Number {{ $order->table_number }}</h3>
        <div class="col-md-10 col-md-offset-1">
          <div class="row">
            <div class="col-md-6">
              <a href="{{ url("/admin/orders/$order->id/menus") }}" class="btn btn-info"><span class="fa fa-plus"></span> Add Menu</a>
            </div>
            <div class="col-md-6">
              <a href="{{ url('/admin/orders') }}" class="btn btn-default btn-back">
                <i class="fa fa-chevron-left"></i> Back
              </a>
            </div>
          </div>
          @include('flash::message')
          <table class="table table-bordered">
            <thead>
            <tr>
              <th class="text-center">No</th>
              <th>Menu</th>
              <th>Note</th>
              <th class="text-center">Qty</th>
              <th>Price</th>
              <th>Sub Total</th>
              <th>Status</th>
              <th class="text-center">#</th>
            </tr>
            </thead>
            <tbody>
            @foreach($order->menus as $key => $menu)
              <tr>
                <td class="text-center">{{ $key + 1 }}</td>
                <td>{{ $menu->name }}</td>
                <td>{{ $menu->pivot->note }}</td>
                <td class="text-center">{{ $menu->pivot->qty }}</td>
                <td>{{ price($menu->pivot->price) }}</td>
                <td>{{ price($menu->pivot->total_price) }}</td>
                <td>{{ getOrderStatus($menu->pivot->is_done) }}</td>
                <td class="text-center">
                  <a href="{{ url('/admin/orders/' . $order->id . '/menus/' . $menu->id . '/edit') }}" class="btn btn-default">Edit</a>
                  @if(!$menu->pivot->is_done)
                  <a href="{{ url('/admin/orders/' . $order->id . '/menus/' . $menu->id . '/done') }}" class="btn btn-success">Done</a>
                  @else
                  <a href="{{ url('/admin/orders/' . $order->id . '/menus/' . $menu->id . '/undone') }}" class="btn btn-warning">Undone</a>
                  @endif
                  <a href="{{ url('/admin/orders/' . $order->id . '/menus/' . $menu->id . '/cancel') }}" class="btn btn-danger">Cancel</a>
                </td>
              </tr>
            @endforeach
            <tr>
              <td colspan="5" class="text-right">Total Price</td>
              <td>{{ price($totalPrice) }}</td>
              <td></td>
              <td></td>
            </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
  <!-- banner -->
@endsection

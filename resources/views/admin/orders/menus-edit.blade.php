@extends('layouts.admin-main')

@section('breadcrumb')
  <li>Orders <span>|</span></li>
  <li>Menus <span>|</span></li>
  <li>Edit</li>
@endsection

@section('content')
  <!-- banner -->
  <div class="banner">
    @include('layouts.admin-sidebar')
    <div class="w3l_banner_nav_right">
      <div class="w3ls_w3l_banner_nav_right_grid w3ls_w3l_banner_nav_right_grid_veg"
           style="border-top: 1px solid #B58F62; padding-top: 50px">
        <h3 class="w3l_fruit">Add Menu</h3>
        <div class="col-md-6 col-md-offset-3">
          <form action="{{ url("/admin/orders/$order->id/menus/$menu->id") }}" method="POST">
            {{ csrf_field() }}
            {{ method_field('PUT') }}
            <div class="form-group">
              <label>Name</label>
              <p>{{ $menu->name }}</p>
            </div>
            <div class="form-group">
              <label>Price</label>
              <p>{{ price($menu->price) }}</p>
            </div>
            <div class="row">
              <div class="col-sm-10">
                <div class="form-group{{ $errors->has('note') ? ' has-error' : '' }}">
                  <label>Note</label>
                  <textarea name="note" class="form-control">{{ old('note', $menu->pivot->note) }}</textarea>
                  @if ($errors->has('note'))
                    <span class="help-block">
                      <strong>{{ $errors->first('note') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="col-sm-2">
                <div class="form-group{{ $errors->has('qty') ? ' has-error' : '' }}">
                  <label>Qty</label>
                  <input type="number" name="qty" class="form-control" value="{{ old('qty', $menu->pivot->qty) }}">
                </div>
              </div>
            </div>
            <button class="btn btn-success" type="submit">Save</button>
            <a href="{{ url("/admin/orders/$order->id") }}" class="btn btn-default" type="submit">Cancel</a>
          </form>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
  <!-- banner -->
@endsection

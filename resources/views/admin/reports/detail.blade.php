@extends('layouts.admin-main')

@section('breadcrumb')
  <li>Orders <span>|</span></li>
  <li>Detail</li>
@endsection

@section('content')
  <!-- banner -->
  <div class="banner">
    @include('layouts.admin-sidebar')
    <div class="w3l_banner_nav_right">
      <div class="w3ls_w3l_banner_nav_right_grid w3ls_w3l_banner_nav_right_grid_veg" style="border-top: 1px solid #B58F62; padding-top: 50px">
        <h3 class="w3l_fruit">Detail Report</h3>
        <div class="col-md-10 col-md-offset-1">
          <a href="{{ url('/admin/reports?type=') }}{{ $day ? 'day' : 'month' }}" class="btn btn-default btn-back">
            <i class="fa fa-chevron-left"></i> Back
          </a>
          <table class="table table-bordered">
            <thead>
            <tr>
              <th>No</th>
              <th>Table Number</th>
              <th>Name</th>
              <th>Price</th>
              <th>Date</th>
              @if($day)
              <th>Time</th>
              @endif
              <th class="text-center">#</th>
            </tr>
            </thead>
            <tbody>
            @forelse($orders as $key => $order)
              <tr>
                <td class="text-center">{{ ($orders->currentPage() - 1) * $orders->perPage() + ($key + 1) }}</td>
                <td class="text-center">{{ $order->table_number }}</td>
                <td>{{ $order->name }}</td>
                <td>{{ price($order->price) }}</td>
                <td>{{ prettyDate($order->created_at) }}</td>
                @if($day)
                  <td class="text-center">{{ prettyTime($order->created_at) }}</td>
                @endif
                <td class="text-center">
                  <a href="{{ url("/admin/transactions/$order->id?year=$year&month=$month") }}{{ $day ? "&day=$day" : '' }}" class="btn btn-default">
                    Show
                  </a>
                </td>
              </tr>
            @empty
              <tr>
                <td class="text-center" colspan="5">Tidak ada pesanan</td>
              </tr>
            @endforelse
            </tbody>
          </table>
          <div class="text-center">
            {!! $orders->links() !!}
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
  <!-- banner -->
@endsection

@extends('layouts.admin-main')

@section('breadcrumb')
  <li>Orders</li>
@endsection

@section('content')
  <!-- banner -->
  <div class="banner">
    @include('layouts.admin-sidebar')
    <div class="w3l_banner_nav_right">
      <div class="w3ls_w3l_banner_nav_right_grid w3ls_w3l_banner_nav_right_grid_veg" style="border-top: 1px solid #B58F62; padding-top: 50px">
        <h3 class="w3l_fruit">Reports</h3>
        <div class="col-md-6 col-md-offset-3">
          <div class="row">
            <div class="col-md-3 col-md-offset-9">
              <div class="form-group">
                <select name="type" class="form-control">
                  <option value="day">Day</option>
                  <option value="month" {{ $type == 'month' ? 'selected' : '' }}>Month</option>
                </select>
              </div>
            </div>
          </div>
          <table class="table table-bordered">
            <thead>
            <tr>
              <th>No</th>
              <th>Count</th>
              <th>{{ $type == 'month' ? 'Month' : 'Date' }}</th>
              <th>Total</th>
              <th class="text-center">#</th>
            </tr>
            </thead>
            <tbody>
            @forelse($reports as $key => $report)
              <tr>
                <td class="text-center">{{ $key + 1 }}</td>
                <td class="text-center">{{ $report->count }}</td>
                <td class="text-center">{{ $report->date }}</td>
                <td>{{ price($report->total_price) }}</td>
                <td class="text-center">
                  <a href="{{ url("/admin/reports/detail?year=$report->year&month=$report->month") }}{{ $report->day ? "&day=$report->day" : '' }}" class="btn btn-default">
                    Show
                  </a>
                </td>
              </tr>
            @empty
              <tr>
                <td class="text-center" colspan="5">Tidak ada laporan</td>
              </tr>
            @endforelse
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
  <!-- banner -->
@endsection

@push('scripts')

<script>
  $('select[name=type]').change(function () {
     window.location = '{{ url('admin/reports?type=') }}' + $(this).val();
  });
</script>

@endpush

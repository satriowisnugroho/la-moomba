@extends('layouts.admin-main')

@section('breadcrumb')
  <li>Recommendations</li>
@endsection

@section('content')
  <!-- banner -->
  <div class="banner">
    @include('layouts.admin-sidebar')
    <div class="w3l_banner_nav_right">
      <div class="w3ls_w3l_banner_nav_right_grid w3ls_w3l_banner_nav_right_grid_veg" style="border-top: 1px solid #B58F62; padding-top: 50px">
        <h3 class="w3l_fruit">Recommendations</h3>
        <div class="menu-button">
          <a href="{{ url('/admin/calculate') }}" class="btn btn-info">Calculate</a>
        </div>
        <div class="col-md-12">
          <table class="table table-bordered">
            <thead>
            <tr>
              <th>No</th>
              <th>Antecedent</th>
              <th>Consequent</th>
              <th>Support</th>
              <th>Confidence</th>
            </tr>
            </thead>
            <tbody>
            @forelse($recommendations as $key => $recommendation)
              <tr>
                <td class="text-center">{{ ($recommendations->currentPage() - 1) * $recommendations->perPage() + ($key + 1) }}</td>
                <td>{{ $recommendation->antecedent->name }}</td>
                <td>{{ $recommendation->consequent->name }}</td>
                <td>{{ $recommendation->support }}</td>
                <td>{{ $recommendation->confidence }}</td>
              </tr>
            @empty
              <tr>
                <td class="text-center" colspan="5">No recommendation found</td>
              </tr>
            @endforelse
            </tbody>
          </table>
          <div class="text-center">
            {!! $recommendations->links() !!}
          </div>
        </div>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
  <!-- banner -->
@endsection

@extends('layouts.admin-main')

@section('head')
  <style>
    td, th {
      text-align: center !important;
    }

    .pull-left-edit {
      -webkit-box-align: center;
      -ms-flex-align: center;
      align-items: center;
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      float: none !important;
      margin-right: 20px;
    }

    .fix-row {
      -webkit-box-align: stretch;
      -ms-flex-align: stretch;
      align-items: stretch;
      display: -webkit-box;
      display: -ms-flexbox;
      display: flex;
      -ms-flex-negative: 0;
      flex-shrink: 0;
      overflow-x: auto;
      height: 415px;
      margin: 0 15px 50px 15px;
    }
  </style>
@endsection

@section('content')
  <!-- banner -->
  <div class="banner">
    @include('layouts.admin-sidebar')
    <div class="w3l_banner_nav_right">
      <div class="w3ls_w3l_banner_nav_right_grid w3ls_w3l_banner_nav_right_grid_veg"
           style="border-top: 1px solid #B58F62; padding-top: 50px">
        <h3 class="w3l_fruit">Calculate</h3>

        <div class="col-md-12">
          <table class="table table-bordered" id="calculate-table">
            <thead>
            <tr>
              <th>Transaksi</th>
              @foreach($items as $item)
                <th>{{ $item->name }}</th>
              @endforeach
            </tr>
            </thead>
            <tbody>
            @foreach($transactions as $key => $transaction)
              <tr>
                <td>{{ $key + 1 }}</td>
                @foreach($items as $item)
                  @php(
                  $found = $transaction['menus']->filter(function ($menu) use ($item) {
                    return $menu->id == $item->id;
                  })
                  )
                  @php($found->count() ? ++$item->total : 0)
                  <td>{{ $found->count() }}</td>
                @endforeach
              </tr>
            @endforeach
            <tr>
              <th>Total</th>
              @foreach($items as $item)
                <th>{{ $item->total }}</th>
              @endforeach
            </tr>
            </tbody>
          </table>
        </div>
      </div>

      <div class="fix-row" style="width: 1000px;">
        @for($i = 0; $i < $items->count() - 1; $i++)
          @for($j = $i + 1; $j < $items->count(); $j++)
            <div class="pull-left-edit">
              <table class="table table-bordered">
                <thead>
                <tr>
                  <th>T</th>
                  <th>{{ $items[$i]->name }}</th>
                  <th>{{ $items[$j]->name }}</th>
                  <th>f</th>
                </tr>
                </thead>
                <tbody>
                @php($total = 0)
                @foreach($transactions as $key => $transaction)
                  <?php
                  $found1 = $transaction['menus']->filter(function ($menu) use ($items, $i) {
                    return $menu->id == $items[$i]->id;
                  });
                  $found2 = $transaction['menus']->filter(function ($menu) use ($items, $j) {
                    return $menu->id == $items[$j]->id;
                  });
                  ?>
                  @php($total += $found1->count() && $found1->count() == $found2->count() ? 1 : 0)
                  <tr>
                    <td>{{ $key + 1 }}</td>
                    <td>{{ $found1->count() }}</td>
                    <td>{{ $found2->count() }}</td>
                    <td>{{ $found1->count() && $found1->count() == $found2->count() ?  'P' : 'S' }}</td>
                  </tr>
                @endforeach
                <tr>
                  <td colspan="3">Σ</td>
                  <td>{{ $total }}</td>
                </tr>
                </tbody>
              </table>
            </div>
          @endfor
        @endfor
      </div>

      <div class="w3ls_w3l_banner_nav_right_grid w3ls_w3l_banner_nav_right_grid_veg">
        <div class="col-md-12">
          <table class="table table-bordered">
            <thead>
            <tr>
              <th>If antecedent then consequent</th>
              <th>Support</th>
              <th>Confidence</th>
            </tr>
            </thead>
            <tbody>
            @for($i = 0; $i < $items->count() - 1; $i++)
              @for($j = $i + 1; $j < $items->count(); $j++)
                @php($t1 = 0)
                @php($t2 = 0)
                @php($total = 0)
                @foreach($transactions as $key => $transaction)
                  <?php
                  $found1 = $transaction['menus']->filter(function ($menu) use ($items, $i) {
                    return $menu->id == $items[$i]->id;
                  });
                  $found2 = $transaction['menus']->filter(function ($menu) use ($items, $j) {
                    return $menu->id == $items[$j]->id;
                  });
                  ?>
                  @php($t1 += $found1->count())
                  @php($t2 += $found2->count())
                  @php($total += $found1->count() && $found1->count() == $found2->count() ? 1 : 0)
                @endforeach
                <tr>
                  <td>If buy <b>{{ $items[$i]->name }}</b> then buy <b>{{ $items[$j]->name }}</b></td>
                  <td>{{ ($total / $transactions->count()) * 100 }}</td>
                  <td>{{ ($total / $t1) * 100 }}</td>
                </tr>
                <tr>
                  <td>If buy <b>{{ $items[$j]->name }}</b> then buy <b>{{ $items[$i]->name }}</b></td>
                  <td>{{ ($total / $transactions->count()) * 100 }}</td>
                  <td>{{ ($total / $t2) * 100 }}</td>
                </tr>
              @endfor
            @endfor
            </tbody>
          </table>
        </div>
      </div>

    </div>
    <div class="clearfix"></div>
  </div>
  <!-- banner -->
@endsection

@push('scripts')

<script>
  var width = 'width: ' + $('#calculate-table').width() + 'px;';
  var height = 'height: ' + $('#calculate-table').height() + 'px';
  $('.fix-row').attr('style', width + height);
</script>

@endpush

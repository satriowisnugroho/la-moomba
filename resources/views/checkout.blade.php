@extends('layouts.main')

@section('breadcrumb')
  <li><span>|</span>Checkout</li>
@endsection

@section('content')
  <!-- banner -->
  <div class="banner">
    @include('layouts.sidebar')
    <div class="w3l_banner_nav_right">
      <!-- about -->
      <div class="privacy about">
        <h3>Chec<span>kout</span></h3>

        <div class="checkout-right">
          <table class="timetable_sub">
            <thead>
            <tr>
              <th>No</th>
              <th>Menu</th>
              <th>Qty</th>
              <th>Name</th>
              <th>Price</th>
              <th>Note</th>
              <th>Remove</th>
            </tr>
            </thead>
            <tbody id="cart-body"></tbody>
          </table>
        </div>
        <div class="checkout-left">
          <div class="col-md-4 checkout-left-basket">
            <h4 style="background: #292929;">Total</h4>
            <ul id="cart-total"></ul>
          </div>
          <div class="col-md-3">
            <form action="{{ url('payment') }}" id="order-form" method="post" class="creditly-card-form agileinfo_form">

              {{ csrf_field() }}
              <input type="hidden" name="menu_ids">
              <input type="hidden" name="quantities">
              <input type="hidden" name="notes">

              <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label class="control-label">Name: </label>
                <input type="text" class="form-control" name="name">
                @if ($errors->has('name'))
                  <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                  </span>
                @endif
              </div>

              <div class="form-group{{ $errors->has('table_number') ? ' has-error' : '' }}">
                <label class="control-label">Table Number: </label>
                <input type="text" class="form-control" name="table_number">
                @if ($errors->has('table_number'))
                  <span class="help-block">
                    <strong>{{ $errors->first('table_number') }}</strong>
                  </span>
                @endif
              </div>

              <button class="btn order-form" id="order" type="button">
                Order <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
              </button>
            </form>
          </div>

          <div class="clearfix"></div>

        </div>

      </div>
      <!-- //about -->
    </div>
    <div class="clearfix"></div>
  </div>
  <!-- //banner -->
@endsection

@push('scripts')

@include('layouts.templates.cart_item')

<script src="{{ asset('js/sweetalert.min.js') }}"></script>
<script>
  $(document).ready(function () {
    var source = $("#cart-item").html();
    var template = Handlebars.compile(source);

    var total = 0;
    paypal.minicart.cart.items().forEach(function (item, key) {
      var data = item._data;
      data.id = key;
      data.key = key + 1;
      var subTotal = data.amount * data.quantity;
      total += subTotal;
      $('#cart-body').append(template(data));
      $('#cart-total').append(
        '<li>' + data.item_name +' <i>-</i> ' + data.quantity +
        ' <span>' + subTotal + '</span></li>'
      );

      $('.close1').off('click');
      $('.close1').click(function () {
        var parent = $(this).parent().parent().parent();
        var id = parent.find('.id').val();
        parent.fadeOut('slow', function () {
          paypal.minicart.cart.remove(id);
          parent.remove();
        });
      });
    });

    $('#cart-total').append('<li class="cart-total">Total <span>' + total + '</span></li>');

    $('#order').click(function (e) {
      e.preventDefault();

      swal({
        title: "Are you sure?",
        text: "After checkout, you will not be able to add menus! Please contact the cashier if you want to add a menu.",
        icon: "warning",
        buttons: true,
        dangerMode: true
      })
        .then(function (willDelete) {
          if (willDelete) {
            var menuIds = paypal.minicart.cart.items().map(function(val) {
              return val._data.menu_id;
            });

            var quantities = paypal.minicart.cart.items().map(function(val) {
              return val._data.quantity;
            });

            var notes = $('input[name^="order-notes"]').map(function () {
              return $(this).val();
            }).get();

            $('input[name="menu_ids"]').val(menuIds);
            $('input[name="quantities"]').val(quantities);
            $('input[name="notes"]').val(notes);

            $('#order-form').submit();
          }
        });
    });
  });
</script>

@endpush

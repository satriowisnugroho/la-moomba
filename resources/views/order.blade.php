@extends('layouts.main')

@section('breadcrumb')
  <li><span>|</span>Order</li>
@endsection

@section('content')
  <!-- banner -->
  <div class="banner">
    @include('layouts.sidebar')
    <div class="w3l_banner_nav_right">
      <!-- about -->
      <div class="privacy about">
        <h3>Order</h3>

        <div class="checkout-right">
          @include('flash::message')
          <table class="timetable_sub">
            <thead>
            <tr>
              <th>No</th>
              <th>Menu</th>
              <th>Qty</th>
              <th>Name</th>
              <th>Price</th>
              <th>Note</th>
            </tr>
            </thead>
            <tbody>
            @foreach($order->menus as $key => $menu)
              <tr class="rem">
                <td class="invert">{{ $key + 1 }}</td>
                <td class="invert-image">
                  <a href="{{ url('menus/' . $menu->id . '/detail') }}">
                    <img src="{{ asset('images/menus/' . $menu->image) }}" class="img-responsive">
                  </a>
                </td>
                <td class="invert">
                  <div class="quantity">
                    <div class="quantity-select">
                      <div class="entry value"><span style="color: #000;">{{ $menu->pivot->qty }}</span></div>
                    </div>
                  </div>
                </td>
                <td class="invert">{{ $menu->name }}</td>
                <td class="invert">{{ price($menu->price) }}</td>
                <td class="invert">
                  <p>{{ $menu->pivot->note }}</p>
                </td>
              </tr>
            @endforeach
            </tbody>
          </table>
        </div>
        <div class="checkout-left">
          <div class="col-md-4 checkout-left-basket">
            <h4 style="background: #666;">Total</h4>
            <ul>
              @php($total = 0)
              @foreach($order->menus as $menu)
                <li>{{ $menu->name }} <i>-</i> {{ $menu->pivot->qty }}
                  <span>{{ price($menu->price * $menu->pivot->qty) }}</span>
                </li>
                @php($total += $menu->price * $menu->pivot->qty)
              @endforeach
              <li class="cart-total">Total <span>{{ price($total) }}</span></li>
            </ul>
          </div>
          <div class="col-md-3">
            <form action="{{ url('print') }}" id="order-form" method="post" class="creditly-card-form agileinfo_form">
              {{ csrf_field() }}
              <button class="btn order-form" id="order">
                Download Invoice <span class="fa fa-save" aria-hidden="true"></span>
              </button>
            </form>
          </div>

          <div class="clearfix"></div>

        </div>

      </div>
      <!-- //about -->
    </div>
    <div class="clearfix"></div>
  </div>
  <!-- //banner -->
@endsection

<!doctype html>
<html lang="id">
<head>
  <title>Invoice</title>
  @include('layouts.bootstrap')
  <style>
    .invoice-title h2, .invoice-title h3 {
      display: inline-block;
    }

    .table > tbody > tr > .no-line {
      border-top: none;
    }

    .table > thead > tr > .no-line {
      border-bottom: none;
    }

    .table > tbody > tr > .thick-line {
      border-top: 2px solid;
    }
  </style>
</head>
<body>
<div class="container">
  <div class="row">
    <div class="col-xs-12">
      <div class="invoice-title">
        <h2>Invoice</h2>
        <h3 class="pull-right">Order # {{ $order->id }}</h3>
      </div>
      <hr>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-6">
      <address>
        <strong>Billed To:</strong><br>
        {{ $order->name }}
      </address>
    </div>
    <div class="col-xs-6">
      <address>
        <strong>Payment Method:</strong><br>
        Cash
      </address>
    </div>
  </div>

  <div class="row">
    <div class="col-xs-6">
      <address>
        <strong>Table Number:</strong><br>
        {{ $order->table_number }}
      </address>
    </div>
    <div class="col-xs-6">
      <address>
        <strong>Order Date:</strong><br>
        {{ prettyDate($order->created_at) }}<br><br>
      </address>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title"><strong>Order summary</strong></h3>
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-condensed">
              <thead>
              <tr>
                <td><strong>Item</strong></td>
                <td class="text-center"><strong>Price</strong></td>
                <td class="text-center"><strong>Quantity</strong></td>
                <td class="text-right"><strong>Totals</strong></td>
              </tr>
              </thead>
              <tbody>
              @php($total = 0)
              @foreach($order->menus as $menu)
                <tr>
                  <td>{{ $menu->name }}</td>
                  <td class="text-center">{{ price($menu->price) }}</td>
                  <td class="text-center">{{ $menu->pivot->qty }}</td>
                  <td class="text-right">{{ price($menu->price * $menu->pivot->qty) }}</td>
                </tr>
                @php($total += $menu->price * $menu->pivot->qty)
              @endforeach
              <tr>
                <td class="thick-line"></td>
                <td class="thick-line"></td>
                <td class="thick-line text-center"><strong>Total</strong></td>
                <td class="thick-line text-right">{{ price($total) }}</td>
              </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>

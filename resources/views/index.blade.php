@extends('layouts.main')

@section('content')
  <!-- //header -->
  <!-- banner -->
  <div class="banner">
    @include('layouts.sidebar')
    <div class="w3l_banner_nav_right">
      <section class="slider">
        <div class="flexslider">
          <ul class="slides">
            @foreach($banners as $banner)
              <li>
                <div class="w3l_banner_nav_right_banner" style="background:url({{ asset('images/menus/' . $banner->banner_image) }}) no-repeat 0px 0px;">
                  <h3>{{ $banner->banner_content }}</h3>
                  <div class="more">
                    <a href="{{ url('menus/' . $banner->id . '/detail') }}" class="button--saqui button--round-l button--text-thick" data-text="Shop now">
                      Shop now
                    </a>
                  </div>
                </div>
              </li>
            @endforeach
          </ul>
        </div>
      </section>
      <!-- flexSlider -->
      <link rel="stylesheet" href="{{ asset('css/flexslider.css') }}" type="text/css" media="screen" property=""/>
      <script defer src="{{ asset('js/jquery.flexslider.js') }}"></script>
      <script type="text/javascript">
        $(window).load(function () {
          $('.flexslider').flexslider({
            animation: "slide",
            start: function (slider) {
              $('body').removeClass('loading');
            }
          });
        });
      </script>
      <!-- //flexSlider -->
    </div>
    <div class="clearfix"></div>
  </div>
  <!-- top-brands -->
  <div class="top-brands">
    <div class="container">
      <h3>Recommended</h3>
      <div class="agile_top_brands_grids">
        @foreach($menus as $menu)
        <div class="col-md-3 top_brand_left">
          <div class="hover14 column">
            <div class="agile_top_brand_left_grid">
              <div class="agile_top_brand_left_grid1">
                <figure>
                  <div class="snipcart-item block">
                    <div class="snipcart-thumb">
                      <a href="{{ url('menus/' . $menu->id . '/detail') }}"><img src="{{ asset('images/menus/' . $menu->image) }}" alt=" " class="img-responsive" /></a>
                      <p>{{ $menu->name }}</p>
                      <h4>{{ price($menu->price) }}</h4>
                    </div>
                    <div class="snipcart-details top_brand_home_details">
                      <form action="#" method="post">
                        <fieldset>
                          <input type="hidden" name="cmd" value="_cart" />
                          <input type="hidden" name="add" value="1" />
                          <input type="hidden" name="business" value=" " />
                          <input type="hidden" name="menu_id" value="{{ $menu->id }}" />
                          <input type="hidden" name="item_name" value="{{ $menu->name }}" />
                          <input type="hidden" name="amount" value="{{ $menu->price }}" />
                          <input type="hidden" name="href" value="{{ url('menus/' . $menu->id) }}" />
                          <input type="hidden" name="image" value="{{ asset('images/menus/' . $menu->image) }}" />
                          <input type="hidden" name="currency_code" value="IDR" />
                          <input type="hidden" name="return" value=" " />
                          <input type="hidden" name="cancel_return" value=" " />
                          <input type="submit" name="submit" value="Add to cart" class="button" />
                        </fieldset>
                      </form>
                    </div>
                  </div>
                </figure>
              </div>
            </div>
          </div>
        </div>
        @endforeach
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
  <!-- //top-brands -->
@endsection

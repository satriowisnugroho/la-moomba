<script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.5/handlebars.min.js"></script>

<script id="cart-item" type="text/x-handlebars-template">
  <tr class="rem">
    <input type="hidden" class="id" value="@{{id}}">
    <td class="invert">@{{key}}</td>
    <td class="invert-image">
      <a href="@{{href}}">
        <img src="@{{image}}" class="img-responsive">
      </a>
    </td>
    <td class="invert">
      <div class="quantity">
        <div class="quantity-select">
          <div class="entry value"><span style="color: #000;">@{{quantity}}</span></div>
        </div>
      </div>
    </td>
    <td class="invert">@{{item_name}}</td>
    <td class="invert">@{{amount}}</td>
    <td class="invert">
      <input type="text" class="form-control" name="order-notes[]">
    </td>
    <td class="invert">
      <div class="rem">
        <div class="close1"></div>
      </div>
    </td>
  </tr>
</script>

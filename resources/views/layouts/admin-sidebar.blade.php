<div class="w3l_banner_nav_left">
  <nav class="navbar nav_bottom">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header nav_2">
      <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse"
              data-target="#bs-megadropdown-tabs">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
      <ul class="nav navbar-nav nav_1">
        @if(auth()->user()->is_admin)
        <li class="{{ \Request::is('admin/dashboard') ? 'active' : '' }}">
          <a href="{{ url('/admin/dashboard') }}">Dashboard</a>
        </li>
        <li class="{{ \Request::is('admin/menus*') ? 'active' : '' }}">
          <a href="{{ url('/admin/menus') }}">Menus</a>
        </li>
        @endif
        <li class="{{ \Request::is('admin/orders*') ? 'active' : '' }}">
          <a href="{{ url('/admin/orders') }}">Orders <span class="new-order hide">0</span></a>
        </li>
        @if(auth()->user()->is_admin)
        <li class="{{ \Request::is('admin/transactions*') ? 'active' : '' }}">
          <a href="{{ url('/admin/transactions') }}">Transactions</a>
        </li>
        <li class="{{ \Request::is('admin/reports*') ? 'active' : '' }}">
          <a href="{{ url('/admin/reports') }}">Reports</a>
        </li>
        <li class="{{ \Request::is('admin/recommendations') ? 'active' : '' }}">
          <a href="{{ url('/admin/recommendations') }}">Recommendations</a>
        </li>
        @endif
      </ul>
    </div><!-- /.navbar-collapse -->
  </nav>
</div>

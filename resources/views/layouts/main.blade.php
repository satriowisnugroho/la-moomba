<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
  <title>La Moomba</title>
  <!-- for-mobile-apps -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta name="keywords" content="Grocery Store Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design"/>
  <script type="application/x-javascript"> addEventListener("load", function () {
      setTimeout(hideURLbar, 0);
    }, false);
    function hideURLbar() {
      window.scrollTo(0, 1);
    } </script>
  <!-- //for-mobile-apps -->
  <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all"/>
  <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" media="all"/>
  <!-- font-awesome icons -->
  <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet" type="text/css" media="all"/>
  <!-- //font-awesome icons -->
  <!-- js -->
  <script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
  <!-- //js -->
  <link href='//fonts.googleapis.com/css?family=Ubuntu:400,300,300italic,400italic,500,500italic,700,700italic'
        rel='stylesheet' type='text/css'>
  <link
    href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
    rel='stylesheet' type='text/css'>
  <!-- start-smoth-scrolling -->
  <script type="text/javascript" src="{{ asset('js/move-top.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/easing.js') }}"></script>
  <script type="text/javascript">
    jQuery(document).ready(function ($) {
      $(".scroll").click(function (event) {
        event.preventDefault();
        $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
      });
    });
  </script>
  <!-- start-smoth-scrolling -->
</head>

<body>
<!-- header -->
<div class="agileits_header">
  <div class="w3l_offers">
    <a href="{{ url('specials') }}">Today's special Offers !</a>
  </div>
  <div class="w3l_search">
    <form action="{{ url('menus') }}">
      <input type="text" name="q" value="Search a foods..." onfocus="this.value = '';"
             onblur="if (this.value == '') {this.value = 'Search a product...';}" required="">
      <input type="submit" value=" ">
    </form>
  </div>
  <div class="product_list_header">
    @if(!\Request::is('checkout') && !session('order_id'))
    <form action="#" method="post" class="last">
      <fieldset>
        <input type="hidden" name="cmd" value="_cart"/>
        <input type="hidden" name="display" value="1"/>
        <input type="submit" name="submit" value="View your cart" class="button"/>
      </fieldset>
    </form>
    @endif
    @if(!\Request::is('order') && session('order_id'))
    <a href="#" id="show-order">
      <input type="submit" name="submit" value="View your order" class="button" style="background: none; padding: .5em 1em .5em 1em"/>
    </a>
    @endif
  </div>
  <div class="clearfix"></div>
</div>
<!-- script-for sticky-nav -->
<script>
  $(document).ready(function () {
    var navoffeset = $(".agileits_header").offset().top;
    $(window).scroll(function () {
      var scrollpos = $(window).scrollTop();
      if (scrollpos >= navoffeset) {
        $(".agileits_header").addClass("fixed");
      } else {
        $(".agileits_header").removeClass("fixed");
      }
    });

  });
</script>
<!-- //script-for sticky-nav -->
<div class="products-breadcrumb">
  <div class="row fix-row">
    <div class="col-xs-2">
      <a href="{{ url('/') }}" style="color: #333"><h2 class="text-center">La Moomba</h2></a>
    </div>
    <div class="col-xs-10 p-breadcrumb">
      <ul>
        <li><i class="fa fa-home" aria-hidden="true"></i><a href="{{ url('/') }}">Home</a></li>
        @yield('breadcrumb')
      </ul>
    </div>
  </div>
</div>
<!-- //header -->

@yield('content')

<!-- footer -->
<div class="footer">
  <div class="container">
    <div class="wthree_footer_copy">
      <p>© {{ Date('Y') }} La Moomba. All rights reserved | Design by <a href="http://w3layouts.com/">W3layouts</a></p>
    </div>
  </div>
</div>
<!-- //footer -->
<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script>
  $(document).ready(function () {
    $(".dropdown").hover(
      function () {
        $('.dropdown-menu', this).stop(true, true).slideDown("fast");
        $(this).toggleClass('open');
      },
      function () {
        $('.dropdown-menu', this).stop(true, true).slideUp("fast");
        $(this).toggleClass('open');
      }
    );
  });
</script>
<!-- here stars scrolling icon -->
<script type="text/javascript">
  $(document).ready(function () {
    /*
     var defaults = {
     containerID: 'toTop', // fading element id
     containerHoverID: 'toTopHover', // fading element hover id
     scrollSpeed: 1200,
     easingType: 'linear'
     };
     */

    $().UItoTop({easingType: 'easeOutQuart'});
  });
</script>
<!-- //here ends scrolling icon -->
<script src="{{ asset('js/minicart.js') }}"></script>
<script>
  paypal.minicart.render();

  paypal.minicart.cart.on('checkout', function (e) {
    e.preventDefault();

    window.location = '{{ url('checkout') }}';
  });

  $('#show-order').click(function (e) {
    e.preventDefault();

    paypal.minicart.reset();
    window.location = '{{ url('/order') }}';
  });
</script>

@stack('scripts')

</body>
</html>

<!--
author: W3layouts
author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
  <title>La Moomba</title>
  <!-- for-mobile-apps -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <meta name="keywords" content="Grocery Store Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design"/>
  <script type="application/x-javascript"> addEventListener("load", function () {
      setTimeout(hideURLbar, 0);
    }, false);
    function hideURLbar() {
      window.scrollTo(0, 1);
    } </script>
  <!-- //for-mobile-apps -->
  <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet" type="text/css" media="all"/>
  <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" media="all"/>
  <!-- font-awesome icons -->
  <link href="{{ asset('css/font-awesome.css') }}" rel="stylesheet" type="text/css" media="all"/>
  <!-- //font-awesome icons -->
  <!-- js -->
  <script src="{{ asset('js/jquery-1.11.1.min.js') }}"></script>
  <!-- //js -->
  <link href='//fonts.googleapis.com/css?family=Ubuntu:400,300,300italic,400italic,500,500italic,700,700italic'
        rel='stylesheet' type='text/css'>
  <link
    href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
    rel='stylesheet' type='text/css'>
  <!-- start-smoth-scrolling -->
  <script type="text/javascript" src="{{ asset('js/move-top.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/easing.js') }}"></script>
  <script type="text/javascript">
    jQuery(document).ready(function ($) {
      $(".scroll").click(function (event) {
        event.preventDefault();
        $('html,body').animate({scrollTop: $(this.hash).offset().top}, 1000);
      });
    });
  </script>
  <!-- start-smoth-scrolling -->
  <style>
    .products-breadcrumb {
      margin-top: 0;
    }
  </style>
  @yield('head')
</head>

<body>
@if(auth()->user())
<!-- header -->
<div class="products-breadcrumb">
  <div class="row" style="margin-left: 0; margin-right: 0">
    <div class="col-md-12">
        <div class="col-md-6">
          <ul>
            <li>
              <i class="fa fa-home" aria-hidden="true"></i>
              <a href="{{ url(auth()->user()->is_admin ? '/admin/dashboard' : '/admin/orders') }}">Home</a>
              <span>|</span>
            </li>
            @yield('breadcrumb')
          </ul>
        </div>
        <div class="col-md-6">
          <div class="pull-right">
            <ul style="margin-top: -13px">
              <li class="dropdown profile_details_drop">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-user" aria-hidden="true"></i>
                  {{ auth()->user()->name }}
                </a>
                <div class="mega-dropdown-menu">
                  <div class="w3ls_vegetables">
                    <ul class="dropdown-menu drp-mnu">
                      <li>
                        <form action="{{ url('logout') }}" method="POST" id="form-logout">
                          {{ csrf_field() }}
                        </form>
                        <a href="#" id="btn-logout">Logout</a>
                      </li>
                    </ul>
                  </div>
                </div>
              </li>
            </ul>
          </div>
        </div>
    </div>
  </div>
</div>
<!-- //header -->
@endif

@yield('content')

<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script>
  $(document).ready(function () {
    $(".dropdown").hover(
      function () {
        $('.dropdown-menu', this).stop(true, true).slideDown("fast");
        $(this).toggleClass('open');
      },
      function () {
        $('.dropdown-menu', this).stop(true, true).slideUp("fast");
        $(this).toggleClass('open');
      }
    );
  });
</script>
<!-- here stars scrolling icon -->
<script type="text/javascript">
  $(document).ready(function () {
    /*
     var defaults = {
     containerID: 'toTop', // fading element id
     containerHoverID: 'toTopHover', // fading element hover id
     scrollSpeed: 1200,
     easingType: 'linear'
     };
     */

    $().UItoTop({easingType: 'easeOutQuart'});

  });
</script>
<!-- //here ends scrolling icon -->
<script src="{{ asset('js/minicart.js') }}"></script>
<script>
  paypal.minicart.render();

  paypal.minicart.cart.on('checkout', function (evt) {
    var items = this.items(),
      len = items.length,
      total = 0,
      i;

    // Count the number of each item in the cart
    for (i = 0; i < len; i++) {
      total += items[i].get('quantity');
    }

    if (total < 3) {
      alert('The minimum order quantity is 3. Please add more to your shopping cart before checking out');
      evt.preventDefault();
    }
  });

  $('#btn-logout').click(function () {
      $('#form-logout').submit();
  });
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<script src="https://js.pusher.com/3.1/pusher.min.js"></script>
<script>
  //instantiate a Pusher object with our Credential's key
  var pusher = new Pusher('013aade25eaa669e02e1', {
    cluster: 'mt1',
    encrypted: true
  });

  //Subscribe to the channel we specified in our Laravel Event
  var channel = pusher.subscribe('my-channel');

  //Bind a function to a Event (the full Laravel class)
  channel.bind('my-event', addMessage);

  var order = $('.new-order');

  function addMessage(data) {
    order.removeClass('hide');
    var val = Number(order.html()) + 1;
    order.html(val);
    $.cookie('new-order', val);

    var audio = new Audio('{{ asset('microbounce.mp3') }}');
    audio.play();

    if (window.location.href === '{{ url('admin/orders') }}') {
      setTimeout(function() { location.reload(); }, 2000);
    }
  }

  if ($.cookie('new-order')) {
    order.removeClass('hide');
    order.html($.cookie('new-order'));
  }
</script>

@stack('scripts')

</body>
</html>

<?php

use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        User::create([
            'name' => 'Administrator',
            'username' => 'admin',
            'password' => bcrypt('12345678'),
            'is_admin' => true,
        ]);

        User::create([
            'name' => 'Operator',
            'username' => 'operator',
            'password' => bcrypt('12345678'),
        ]);
    }
}

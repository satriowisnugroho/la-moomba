<?php

use App\Menu;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class MenusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('menus')->delete();

        Menu::create([
            'name' => 'Nasi Goreng',
            'price' => 100000,
            'description' => 'Lorem ipsum dolor sit amet',
            'category' => 'main-course',
            'type' => 'chicken',
            'image' => '34.png',
        ]);

        Menu::create([
            'name' => 'Nasi Liwet',
            'price' => 100000,
            'description' => 'Lorem ipsum dolor sit amet',
            'category' => 'main-course',
            'type' => 'seafood',
            'image' => '37.png',
        ]);

        Menu::create([
            'name' => 'Nasi Balap',
            'price' => 100000,
            'description' => 'Lorem ipsum dolor sit amet',
            'category' => 'main-course',
            'type' => 'beef',
            'image' => '39.png',
        ]);

        Menu::create([
            'name' => 'French Fries',
            'price' => 100000,
            'description' => 'Lorem ipsum dolor sit amet',
            'category' => 'appetizer',
            'type' => null,
            'image' => '41.png',
        ]);

        Menu::create([
            'name' => 'Zupa Soup',
            'price' => 100000,
            'description' => 'Lorem ipsum dolor sit amet',
            'category' => 'appetizer',
            'type' => null,
            'image' => '43.png',
        ]);

        Menu::create([
            'name' => 'Hula Hop',
            'price' => 100000,
            'description' => 'Lorem ipsum dolor sit amet',
            'category' => 'appetizer',
            'type' => null,
            'image' => '44.png',
        ]);

        Menu::create([
            'name' => 'Ice Cone',
            'price' => 100000,
            'description' => 'Lorem ipsum dolor sit amet',
            'category' => 'dessert',
            'type' => null,
            'image' => '45.png',
        ]);

        Menu::create([
            'name' => 'Sundae',
            'price' => 100000,
            'description' => 'Lorem ipsum dolor sit amet',
            'category' => 'dessert',
            'type' => null,
            'image' => '46.png',
        ]);

        Menu::create([
            'name' => 'Pudding Duren',
            'price' => 100000,
            'description' => 'Lorem ipsum dolor sit amet',
            'category' => 'dessert',
            'type' => null,
            'image' => '48.png',
        ]);
    }
}

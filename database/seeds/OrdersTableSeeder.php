<?php

use App\Order;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->delete();

        $order1 = Order::create([
            'name' => 'Lorem ipsum',
            'table_number' => 1,
            'status' => 0,
            'price' => 10000000,
        ]);

        $order1->menus()->attach([1 => ['qty' => 1, 'price' => 10000, 'total_price' => 300000]]);
        $order1->menus()->attach([2 => ['qty' => 2, 'price' => 10000, 'total_price' => 300000]]);
        $order1->menus()->attach([3 => ['qty' => 1, 'price' => 10000, 'total_price' => 300000]]);

        $order2 = Order::create([
            'name' => 'Lorem ipsum',
            'table_number' => 2,
            'status' => 0,
            'price' => 10000000,
        ]);

        $order2->menus()->attach([1 => ['qty' => 2, 'price' => 10000, 'total_price' => 300000]]);
        $order2->menus()->attach([3 => ['qty' => 3, 'price' => 10000, 'total_price' => 300000]]);
        $order2->menus()->attach([4 => ['qty' => 1, 'price' => 10000, 'total_price' => 300000]]);

        $order3 = Order::create([
            'name' => 'Lorem ipsum',
            'table_number' => 3,
            'status' => 0,
            'price' => 10000000,
        ]);

        $order3->menus()->attach([3 => ['qty' => 1, 'price' => 10000, 'total_price' => 300000]]);
        $order3->menus()->attach([4 => ['qty' => 1, 'price' => 10000, 'total_price' => 300000]]);
        $order3->menus()->attach([5 => ['qty' => 1, 'price' => 10000, 'total_price' => 300000]]);

        $order4 = Order::create([
            'name' => 'Lorem ipsum',
            'table_number' => 4,
            'status' => 0,
            'price' => 10000000,
        ]);

        $order4->menus()->attach([1 => ['qty' => 4, 'price' => 10000, 'total_price' => 300000]]);
        $order4->menus()->attach([5 => ['qty' => 1, 'price' => 10000, 'total_price' => 300000]]);
        $order4->menus()->attach([6 => ['qty' => 3, 'price' => 10000, 'total_price' => 300000]]);

        $order5 = Order::create([
            'name' => 'Lorem ipsum',
            'table_number' => 5,
            'status' => 0,
            'price' => 10000000,
        ]);

        $order5->menus()->attach([2 => ['qty' => 2, 'price' => 10000, 'total_price' => 300000]]);
        $order5->menus()->attach([4 => ['qty' => 1, 'price' => 10000, 'total_price' => 300000]]);
        $order5->menus()->attach([5 => ['qty' => 2, 'price' => 10000, 'total_price' => 300000]]);
    }
}

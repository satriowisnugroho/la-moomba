<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecommendationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recommendations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('antecedent_id');
            $table->unsignedInteger('consequent_id');
            $table->foreign('antecedent_id')->references('id')->on('menus')->onDelete('cascade');
            $table->foreign('consequent_id')->references('id')->on('menus')->onDelete('cascade');
            $table->decimal('support');
            $table->decimal('confidence');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recommendations');
    }
}
